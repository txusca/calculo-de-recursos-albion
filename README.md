# Cálculo de recursos Albion

- [ ] Fazer em Java ou JavaScript utilizando Bootstrap
- [ ] Primeiro perguntar se verificoum o gasto de foco por recurso
- [ ] Perguntar qual o tier do recurso
- [ ] Pedir o foco total disponível
- [ ] Pedir o foco gasto por recurso
- [ ] Pedir a taxa de retorno
- [ ] Mostrar o total do recurso craftável e quantos necessitará de cada tier. Exemplo: pelego t5: quantidade obtida pelo cálculo; couro t4: quantidade obtida pelo cálculo
- [ ] Talvez acrescentar melhor local para compra usando o albiondb